package com.example.cobamapsosm;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.osmdroid.api.IMapController;
import org.osmdroid.bonuspack.clustering.RadiusMarkerClusterer;
import org.osmdroid.bonuspack.location.NominatimPOIProvider;
import org.osmdroid.bonuspack.location.POI;
import org.osmdroid.bonuspack.routing.MapQuestRoadManager;
import org.osmdroid.bonuspack.routing.OSRMRoadManager;
import org.osmdroid.bonuspack.routing.Road;
import org.osmdroid.bonuspack.routing.RoadManager;
import org.osmdroid.config.Configuration;
import org.osmdroid.events.MapEventsReceiver;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.BoundingBox;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.ItemizedOverlay;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.MapEventsOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.TilesOverlay;
import org.osmdroid.views.overlay.infowindow.InfoWindow;
import org.osmdroid.views.overlay.infowindow.MarkerInfoWindow;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private MapView map;
    private IMapController mapController;
    BoundingBox boundingBox = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);

        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                Log.v("apaa", "Permission is granted");
            } else {
                Log.v("apaa", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        }

        final StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        setContentView(R.layout.activity_main);
        map = (MapView) findViewById(R.id.map);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setBuiltInZoomControls(true);
        map.setMultiTouchControls(true);
        final IMapController mapController = map.getController();
        mapController.setZoom(15);
        map.setMinZoomLevel(12.0);

//        GeoPoint startPoint = new GeoPoint(-7.912965, 112.598376);
//        IMapController mapController = map.getController();
//        mapController.setZoom(15);
//        mapController.setCenter(startPoint);
//
//        //1. "Hello, Routing World"
//        //RoadManager roadManager = new OSRMRoadManager(this);
//        //2. Playing with the RoadManager
//        RoadManager roadManager = new MapQuestRoadManager("3iyeFAWJEqrcNwo8");
//        roadManager.addRequestOption("routeType=shortest");
//        ArrayList<GeoPoint> waypoints = new ArrayList<GeoPoint>();
//        waypoints.add(startPoint);
//        GeoPoint endPoint = new GeoPoint(-7.921505, 112.597244);
//        waypoints.add(endPoint);
//        waypoints.add(new GeoPoint(-7.919452,112.566345));
//        waypoints.add(new GeoPoint(-7.905926,112.546640));
//        Road road = roadManager.getRoad(waypoints);
//        if (road.mStatus != Road.STATUS_OK)
//            Toast.makeText(this, "Error when loading the road - status=" + road.mStatus, Toast.LENGTH_SHORT).show();
//
//        Polyline roadOverlay = RoadManager.buildRoadOverlay(road);
//        map.getOverlays().add(roadOverlay);
//
//        Marker startMarker = new Marker(map);
//        startMarker.setPosition(startPoint);
//        startMarker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
//        map.getOverlays().add(startMarker);
//
//        Drawable newMarker = this.getResources().getDrawable(R.drawable.home_address);
//        ArrayList<OverlayItem> items = new ArrayList<OverlayItem>();
//        ItemizedIconOverlay<OverlayItem> mOverlay = new ItemizedIconOverlay<OverlayItem>(items,
//                new ItemizedIconOverlay.OnItemGestureListener<OverlayItem>() {
//                    @Override
//                    public boolean onItemSingleTapUp(final int position, final OverlayItem item) {
//                        Toast.makeText(MainActivity.this, ""+position, Toast.LENGTH_SHORT).show();
//                        return true;
//                    }
//                    @Override
//                    public boolean onItemLongPress(final int position, final OverlayItem item) {
//                        Toast.makeText(MainActivity.this, "lama: "+position, Toast.LENGTH_SHORT).show();
//                        return false;
//                    }
//                }, getApplicationContext());

//        mOverlay.addItem(new OverlayItem("Title", "Description", new GeoPoint(-7.912965,112.598376)));
//        mOverlay.addItem(new OverlayItem("Title", "Description", new GeoPoint(-7.921505,112.597244)));
//        mOverlay.addItem(new OverlayItem("Title", "Description", new GeoPoint(-7.919452,112.566345)));
//        mOverlay.addItem(new OverlayItem("Title", "Description", new GeoPoint(-7.905926,112.546640)));
//
//
//        map.getOverlays().add(mOverlay);
//
//        RadiusMarkerClusterer poiMarkers = new RadiusMarkerClusterer(this);
//        //end of 10.
//        //11.1 Customizing the clusters design
//        Drawable clusterIconD = ResourcesCompat.getDrawable(getResources(), R.drawable.marker_poi_cluster, null);
//        Bitmap clusterIcon = ((BitmapDrawable) clusterIconD).getBitmap();
//        poiMarkers.setIcon(clusterIcon);
//        poiMarkers.getTextPaint().setTextSize(12 * getResources().getDisplayMetrics().density);
//        poiMarkers.mAnchorV = Marker.ANCHOR_BOTTOM;
//        poiMarkers.mTextAnchorU = 0.70f;
//        poiMarkers.mTextAnchorV = 0.27f;
//        //end of 11.1
//        map.getOverlays().add(poiMarkers);

        Double[] lat = {-7.912965, -7.921505, -7.919452, -7.905926};
        Double[] lng = {112.598376, 112.597244, 112.566345, 112.546640};
        String[] title = {"coba", "dulu", "sob", "hehe"};


        final List<Overlay> overlays = map.getOverlays();
        overlays.clear();

// create and set up a clusterer
        final RadiusMarkerClusterer clusterer = new RadiusMarkerClusterer(this);
        clusterer.setIcon(BitmapFactory.decodeResource(getResources(), R.drawable.marker_poi_cluster));
        clusterer.setRadius(85);
        clusterer.mTextAnchorU = 0.5f;
        clusterer.mTextAnchorV = 0.5f;
        clusterer.getTextPaint().setTextSize(60.0f);

        Drawable dr = getResources().getDrawable(R.drawable.home_address);
        Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
        Drawable markerDrawable = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 120, 120, true));

// for each entry, create and set up a marker
        for (int i = 0; i < 4; i++) {
            Marker mr = new Marker(map);
            mr.setPosition(new GeoPoint(lat[i], lng[i]));
            mr.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            mr.setTitle(title[i]);
            mr.setIcon(markerDrawable);
            clusterer.add(mr);
            overlays.add(clusterer);

            final int indeks = i;
            mr.setOnMarkerClickListener(new Marker.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(final Marker marker, MapView mapView) {
                    if (marker.isInfoWindowShown()) {
                        InfoWindow.closeAllInfoWindowsOn(mapView);
                    } else {
                        Toast.makeText(MainActivity.this, "posisi " + indeks, Toast.LENGTH_SHORT).show();
                        map.getController().animateTo(new GeoPoint(marker.getPosition()));

                    }
                    return true;
                }
            });


            clusterer.setRadius(200);
        }

        double north = -90;
        double south = 90;
        double west = 180;
        double east = -180;
        for (int i = 0; i < 4; i++) {
            north = Math.max(lat[i], north);
            south = Math.min(lat[i], south);
            west = Math.min(lng[i], west);
            east = Math.max(lng[i], east);
        }

        Log.d("tag aja", String.format("North %f, south %f, west %f, east %f", north, south, west, east));
        boundingBox = new BoundingBox(north, east, south, west);
        map.zoomToBoundingBox(boundingBox, true);
    }


    public void onResume() {
        super.onResume();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));
        map.onResume(); //needed for compass, my location overlays, v6.0.0 and up
    }

    public void onPause() {
        super.onPause();
        //this will refresh the osmdroid configuration on resuming.
        //if you make changes to the configuration, use
        //SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        //Configuration.getInstance().save(this, prefs);
        map.onPause();  //needed for compass, my location overlays, v6.0.0 and up
    }
}
